from django.shortcuts import render
from django.views import View
from django.views.generic.list import ListView
from .models import Therapist
from .forms import TherapistForm
from django.forms import formset_factory

class HomePage(ListView):
    model = Therapist
    TherapistFormSet = formset_factory(TherapistForm)

    paginate_by = 10
    template_name = 'core/base.html'
    extra_context = {'name': 'Therapist', 'formset': TherapistFormSet()}



