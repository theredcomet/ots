# Generated by Django 2.1.7 on 2019-02-27 10:00

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='therapist',
            name='m_type',
            field=models.IntegerField(choices=[(0, 'M'), (1, 'P40'), (2, 'P100')], default=0),
        ),
    ]
