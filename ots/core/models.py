from django.db import models
from django.contrib.auth.base_user import AbstractBaseUser, BaseUserManager


class TherapistManager(BaseUserManager):
    def create_user(self, email, first_name, last_name, password=None):
        """
        Creates and saves a User with the given email, date of
        birth and password.
        """
        if not email:
            raise ValueError('Users must have an email address')

        if not password:
            raise ValueError("Password musg bn")

        user = self.model(
            email=self.normalize_email(email),
            first_name=first_name,
            last_name=last_name
        )

        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_superuser(self, email, first_name, last_name, password):
        """
        Creates and saves a superuser with the given email, date of
        birth and password.
        """
        user = self.create_user(
            email,
            password=password,
            first_name=first_name,
            last_name=last_name
        )
        user.is_admin = True
        user.save(using=self._db)
        return user


class Therapist(AbstractBaseUser):

    M_TYPE = (
        (0, "M"),
        (1, "P40"),
        (2, "P100"),
    )

    first_name = models.CharField(max_length=50)
    last_name = models.CharField(max_length=50)
    slug = models.SlugField(max_length=70, unique=True, null=True)
    email = models.EmailField(unique=True, verbose_name="Email Address")
    m_type = models.IntegerField(choices=M_TYPE, default=0)

    is_active = models.BooleanField(default=True)
    is_admin = models.BooleanField(default=False)
    is_staff = models.BooleanField(default=True)

    objects = TherapistManager()

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = ['first_name', 'last_name', 'password']

    def __str__(self):
        return self.email

    def has_perm(self, perm, obj=None):
        "Does the user have a specific permission?"
        # Simplest possible answer: Yes, always
        return True

    def has_module_perms(self, app_label):
        "Does the user have permissions to view the app `app_label`?"
        # Simplest possible answer: Yes, always
        return True

    # @property
    # def is_staff(self):
    #     "Is the user a member of staff?"
    #     # Simplest possible answer: All admins are staff
    #     return self.is_staff

    @property
    def slug(self):
        return ''.join([
            self.first_name.lower(),
            '-',
            self.last_name.lower(),
            '-',
            str(self.id)])

class Client(models.Model):
    client_name = models.CharField(max_length=100)
    start_date = models.DateTimeField(auto_now_add=True, editable=True)
    end_date = models.DateTimeField(null=True, blank=True)

    source = models.ForeignKey(
        Therapist, on_delete=models.CASCADE)

    referral_date = models.DateTimeField(null=True, blank=True)

    def __str__(self):
        return self.client_name