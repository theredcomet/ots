from django.forms import ModelForm
from .models import Therapist

class TherapistForm(ModelForm):
    class Meta:
        model = Therapist
        fields = ['first_name', 'last_name', 'email', 'password', 'm_type']