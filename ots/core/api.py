from rest_framework import routers, serializers, viewsets
from .models import Therapist

class TherapistSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Therapist
        fields = ("id", "first_name", "last_name", "slug", "email", "password")


class TherapistViewSet(viewsets.ModelViewSet):
    queryset = Therapist.objects.all()
    serializer_class = TherapistSerializer