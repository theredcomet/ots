from django.db import models

from core.models import Therapist, Client

class TherapySession(models.Model):
    therapist = models.ForeignKey(Therapist, on_delete=models.CASCADE)
    client = models.ForeignKey(Client, on_delete=models.CASCADE)

    date = models.DateField()
    fees = models.DecimalField(max_digits=8,decimal_places=2)

    def __str__(self):
        return '{}-{}-{}'.format(self.id, self.client.client_name, self.date)
