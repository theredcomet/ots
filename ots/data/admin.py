from django.contrib import admin
from rangefilter.filter import DateRangeFilter
from .models import TherapySession


class TherapySessionAdmin(admin.ModelAdmin):

    list_display = ('id', 'therapist', 'client', 'date', 'fees')
    list_filter = ('therapist', 'date')
    exclude = ('therapist', )


    def save_model(self, request, obj, form, change):
        instance = form.save(commit=False)
        if not hasattr(instance, 'therapist'):
            instance.therapist = request.user
        instance.save()
        form.save_m2m()
        return instance


admin.site.register(TherapySession, TherapySessionAdmin)