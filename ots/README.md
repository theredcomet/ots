# OTS

django based web-application to handle:
*  Therapists registers
*  Therapists profile
*  Sessions handled by therapists
*  Fees Charges by therapists

## core

*core* has basic model definitions such as user (inherited Abstract Base User) and other important
data such as geographical data.


## data

*data* app has details on clients and sessions handled by the therapists.


## How to run

```python

pip install -r requirements.txt
python manage.py makemigrations
python manage.py migrate
python manage.py runserver

```
