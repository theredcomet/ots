import decode from 'jwt-decode';

let TOKEN = 'id_token';
let REFRESH = 'id_refresh';


export default class AuthService {
    // Initializing important variables
    constructor(domain) {
        this.domain = domain || 'http://localhost:8000' // API server domain
        this.fetch = this.fetch.bind(this) // React binding stuff
        this.login = this.login.bind(this)
        this.getProfile = this.getProfile.bind(this)
    }

    login(email, password) {
        // Get a token from api server using the fetch api
        return this.fetch(`${this.domain}/api/token/`, {
            method: 'POST',
            body: JSON.stringify({
                email: email,
                password: password
            })
        }).then(res => {
            this.setToken(res.access) // Setting the token in localStorage
            this.setRefreshToken(res.refresh) // Setting the token in localStorage
            return Promise.resolve(res);
        })
    }

    refresh() {
        let idRefresh = this.getRefreshToken();
        // Refresh the token pair recieved from api
        return this.fetch(`${this.domain}/api/refresh/`, {
            method: 'POST',
            body: JSON.stringify({
                refresh: idRefresh,
            })
        })
    }

    loggedIn() {
        // Checks if there is a saved token and it's still valid
        const token = this.getToken() // GEtting token from localstorage
        return !!token && !this.isTokenExpired(token) // handwaiving here
    }

    isTokenExpired(token) {
        try {
            const decoded = decode(token);
            if (decoded.exp < Date.now() / 1000) { // Checking if token is expired. N
                return true;
            }
            else

                return false;
        }
        catch (err) {
            return false;
        }
    }

    setToken(idToken) {
        // Saves user token to localStorage
        localStorage.setItem(TOKEN, idToken)
    }

    setRefreshToken(idRefresh) {
        localStorage.setItem(REFRESH, idRefresh);
    }

    getToken() {
        // Retrieves the user token from localStorage
        return localStorage.getItem(TOKEN)
    }


    getRefreshToken() {
        return localStorage.getItem(REFRESH);
    }

    logout() {
        // Clear user token and profile data from localStorage
        localStorage.removeItem(TOKEN);
        localStorage.removeItem(REFRESH);
    }

    getProfile() {
        // Using jwt-decode npm package to decode the token
        return decode(this.getToken());
    }


    fetch(url, options) {
        // performs api calls sending the required authentication headers
        const headers = {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        }

        // Setting Authorization header
        // Authorization: Bearer xxxxxxx.xxxxxxxx.xxxxxx
        if (this.loggedIn()) {
            headers['Authorization'] = 'Bearer ' + this.getToken()
        }

        return fetch(url, {
            headers,
            ...options
        })
            .then(this._checkStatus)
            .then(response => response.json())
    }

    _checkStatus(response) {
        // raises an error in case response status is not a success
        if (response.status >= 200 && response.status < 300) { // Success status lies between 200 to 300
            return response
        } else {
            var error = new Error(response.statusText)
            error.response = response
            throw error
        }
    }
}